#include "printlib.h"

int main() {

	int a,b,c,d;

	a = 5;
	b = 6;

	c = a + b;

    d = b - a;

    println_int(c);
    println_int(d);

	return 0;
}

// EXPECTED
// 11
// 1


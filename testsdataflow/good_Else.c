#include "printlib.h"

int main()
{
  int n;

  n = 6;
  if(false ) {
    n = 5;
  } else {
    n = 8;
  }

  println_int(n);
  return 0;
}

// EXPECTED
// 8
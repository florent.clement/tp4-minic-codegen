#include "printlib.h"

int main()
{

    if( true ) {
        println_int(10);
    }

    if( true || false ) {
        println_int(9);
    }

    if( false || true ) {
        println_int(8);
    }

    if( false || false ) {
        println_int(5);
    } else {
        println_int(6);
    }

  return 0;
}

// EXPECTED
// 10
// 9
// 8
// 6
#include "printlib.h"

int main()
{
  int n,a,f,g;

  n = (6);
  a = 2;
  f = 8;
  g = 15;

  if( n > 5 ) {
    println_int(1);
  }

  if( a >= 2 ) {
    println_int(2);
  }

  if( f <= 8 && f < 9 ) {
    println_int(3);
  }

  if( g <= 20 ) {
    println_int(4);
  }

  if( g > 20 ) {
      println_int(5);
    }

  return 0;
}

// EXPECTED
// 1
// 2
// 3
// 4
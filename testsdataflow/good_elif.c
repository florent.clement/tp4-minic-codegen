#include "printlib.h"

int main()
{
  int n,a;
  n=18;
  if(n == 5) {
    a = 0;
  } else if(n == 18) {
    a = 5;
  } else {
    a = 9;
  }
  println_int(a);
  return 0;
}

// EXPECTED
// 5

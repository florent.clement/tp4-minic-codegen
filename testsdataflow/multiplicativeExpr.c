#include "printlib.h"

int main() {

	println_int(5 * 0);
	println_int(5 * 1);
	println_int(0 * 5);
	println_int(( 1 + 2) * 5);


	println_int(5 / 2);
	println_int(0 / 2);

	println_int(4 % 2);
	println_int(5 % 2);

	return 0;
}

// EXPECTED
// 0
// 5
// 0
// 15
// 2
// 0
// 0
// 1


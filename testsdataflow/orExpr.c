#include "printlib.h"

int main() {

	bool a,b,c,d,e;

	a = true;
	b = true;
	c = false;
	d = false;

    println_int(a || b);
    println_int(a || c);
    println_int(c || d);
    println_int(b || d);

	return 0;
}

// EXPECTED
// 1
// 1
// 0
// 1
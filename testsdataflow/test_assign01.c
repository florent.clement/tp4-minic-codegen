#include "printlib.h"

int main() {

    int a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y;

    a = 1;
    b = 1;
    c = 1;
    d = 1;
    e = 1;
    f = 1;
    g = 1;
    h = 1;
    i = 1;
    j = 1;
    k = 1;
    l = 1;
    m = 1;
    n = 1;
    o = 1;
    p = 1;
    q = 1;
    r = 1;
    s = 1;
    t = 1;
    u = 1;
    v = 1;
    w = 1;
    x = 1;
    y = 1;

    a = 1;
    b = 1 + a;
    c = 1 + b;
    d = 1 + c;
    e = 1 + d;
    f = 1 + e;
    g = 1 + f;
    h = 1 + g;
    i = 1 + h;
    j = 1 + i;
    k = 1 + j;
    l = 1 + k;
    m = 1 + l;
    n = 1 + m;
    o = 1 + n;
    p = 1 + o;
    q = 1 + p;
    r = 1 + q;
    s = 1 + r;
    t = 1 + s;
    u = 1 + t;
    v = 1 + u;
    w = 1 + v;
    x = 1 + w;
    y = 1 + x;

    println_int(a);
    println_int(b);
    println_int(c);
    println_int(d);
    println_int(e);
    println_int(f);
    println_int(g);
    println_int(h);
    println_int(i);
    println_int(j);
    println_int(k);
    println_int(l);
    println_int(m);
    println_int(n);
    println_int(o);
    println_int(p);
    println_int(q);
    println_int(r);
    println_int(s);
    println_int(t);
    println_int(u);
    println_int(v);
    println_int(w);
    println_int(x);
    println_int(y);

	return 0;
}

// EXPECTED
// 1
// 2
// 3
// 4
// 5
// 6
// 7
// 8
// 9
// 10
// 11
// 12
// 13
// 14
// 15
// 16
// 17
// 18
// 19
// 20
// 21
// 22
// 23
// 24
// 25


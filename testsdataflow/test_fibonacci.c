#include "printlib.h"

int main() {

    int a,b,c,cpt;

    cpt = 10;
    a = 1;
    b = 1;

    while(cpt != 0) {
        c = a + b;
        a = b;
        b = c;

        cpt = cpt - 1;
    }

    println_int(c);



	return 0;
}

// EXPECTED
// 144


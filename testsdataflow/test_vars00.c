#include "printlib.h"

int main() {

	int a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v;

	a = 1;
	b = 2;
	c = 3;
	d = 4;
	e = 5;
	f = 6;
	g = 7;
	h = 8;
	i = 9;
	j = 10;
	k = 11;
	l = 12;
	m = 13;
	n = 14;
	o = 15;
	p = 16;
	q = 17;
	r = 18;
	s = 19;
	t = 20;
	u = 21;
	v = 22;

	println_int(a);
	println_int(b);
	println_int(c);
	println_int(d);
	println_int(e);
	println_int(f);
	println_int(g);
	println_int(h);
	println_int(i);
	println_int(j);
	println_int(k);
	println_int(l);
	println_int(m);
	println_int(n);
	println_int(o);
	println_int(p);
	println_int(q);
	println_int(r);
	println_int(s);
	println_int(t);
	println_int(u);
	println_int(v);


	if(a < 10) {
    	println_int(101);
	}

	while(a != 100) {
	    a = a + 1;
    }

    println_int(a);

	return 0;
}

// EXPECTED
// 1
// 2
// 3
// 4
// 5
// 6
// 7
// 8
// 9
// 10
// 11
// 12
// 13
// 14
// 15
// 16
// 17
// 18
// 19
// 20
// 21
// 22
// 101
// 100
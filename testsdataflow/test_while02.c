#include "printlib.h"

int main() {

	int a,b,c,d;

	a = 6;
    b = 0;
    c = 0;
    d = 0;

    while(a != b) {
        if(b % 2 == 0) {
            b = b + 2;
        }

        if( a % 2 == 0 ) {
            c = c + 1;
        } else {
            d = d + 1;
        }
        a = a + 1;
    }

    println_int(b);
    println_int(a);
    println_int(b == a);

    println_int(a * b);


    println_int(c);

	return 0;
}

// EXPECTED
// 12
// 12
// 1
// 144
// 3